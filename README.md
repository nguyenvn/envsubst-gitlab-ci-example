# envsubst-gitlab-ci-example
A simple project showing how to use `envsubst` to populate files with build-time envirnonment variables.

### Details
Checkout the .gitlab-ci.yml and properties.tmpl files for the input state and look at the Gitlab-CI Pipelines for this job to see the output.
